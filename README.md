### Roundcube classic dark theme

This is a quick and dirty attempt to modify the roundcube classic
theme with dark backgrounds and light texts.

To use it put the classic-dark folder contained in this repository
into to your roundcube installation's skins folder.

![](https://gitlab.com/t-5/roundcube-classic-dark/-/raw/master/classic-dark/screenshot.png?ref_type=heads)
