#!/bin/bash

cd classic

echo -n > ../classic-dark.diff

find . -type f \
    -name "*.css" ! -name "*.min.*" \
    -exec echo {} \; \
    -exec diff -u {} ../classic-dark/{} > ../classic-dark.diff \;
