#!/bin/bash

rsync -avp --delete classic/ classic-dark/
rm classic-dark/README
patch -p1 < classic-dark.diff
cp -f _classic_dark_files/glass.png classic-dark/images/icons
cp -f _classic_dark_files/glass_roll.png classic-dark/images/icons
cp -f _classic_dark_files/roundcube_logo.png classic-dark/images
cp -f _classic_dark_files/searchfield.gif classic-dark/images
cp -f _classic_dark_files/taskicons.gif classic-dark/images
cp -f _classic_dark_files/watermark.gif classic-dark/images
cp -f _classic_dark_files/watermark.html classic-dark
cp -f _classic_dark_files/thumbnail.png classic-dark
cp -f _classic_dark_files/meta.json classic-dark
./minify-dark.sh
