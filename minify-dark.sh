#!/bin/bash

find classic-dark/ -type f \
    -name "*.css" ! -name "*.min.*" \
    -exec echo {} \; \
    -exec uglifycss --output {}.min {} \;

cd classic-dark

for a in *.css.min; do
    b=`echo $a|sed 's#\.css\.min#\.min\.css#'`
    mv -f $a $b
done

cd ..
